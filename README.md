## Base de um MVC simples 

A ideia é criar uma estrutura básica de um MVC com um sistema de rotas e authenticação.

## Usar RewriteModule

```
<VirtualHost *:80>
    <Directory /var/www/html/pizzariasystem>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ index.php [QSA,L]
    </Directory>

    . . .
</VirtualHost>

```
## Importar SQL

Criar um BD.
Importar para o BD o arquivo pizzaria.sql.
Alterar o arqivo Classes/Dao/Database.php com os dados do BD utilizado.

