-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 24-Abr-2019 às 18:46
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizzaria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_street` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address_number` int(11) NOT NULL,
  `address_complement` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_observation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_tax` double DEFAULT NULL,
  `costumer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `address`
--

INSERT INTO `address` (`id`, `address_street`, `address_number`, `address_complement`, `address_observation`, `address_tax`, `costumer_id`) VALUES
(1, 'Tv. Antônio de Toledo', 31, 'Casa 4', 'Buzinar', 2, 1),
(2, 'Av. Zumkeller', 98, 'Subsolo', '', 5, 2),
(3, 'Av Tiradentes', 245, 'Ap 02', '', 4.7, 3),
(4, 'Av. Zumkeller', 43, 'Ap 02', '', 1.28, 4),
(5, 'Av. Zumkeller', 57, '', '', 0.5, 5),
(6, 'Av. Zumkeller', 57, '', '', 0.5, 5),
(7, 'Av. Zumkeller', 1, '', '', 1, 6),
(8, 'Tv. Antônio de Toledo', 2, 'Casa 4', 'Tocar no numero 3', 5, 7),
(9, 'Tv. Antonio de Toledo', 2, 'Casa 3', 'Buzinar', 5, 8),
(10, 'Tv. Antonio de Toledo', 31, 'Casa 3', 'Gritar: olha a pizza!', 1, 9);

-- --------------------------------------------------------

--
-- Estrutura da tabela `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(1, 'Pizzas'),
(2, 'Pizzas Doces'),
(3, 'Bebidas'),
(4, 'Calzones'),
(5, 'Esfihas'),
(6, 'Adicionais');

-- --------------------------------------------------------

--
-- Estrutura da tabela `costumer`
--

DROP TABLE IF EXISTS `costumer`;
CREATE TABLE IF NOT EXISTS `costumer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `costumer_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `costumer`
--

INSERT INTO `costumer` (`id`, `costumer_number`) VALUES
(1, '11996299927'),
(2, '1133845594'),
(3, '1122335678'),
(4, '1122334433'),
(5, '1122443322'),
(6, '1144223322'),
(7, '1155559999'),
(8, '1122223333'),
(9, '1123456789');

-- --------------------------------------------------------

--
-- Estrutura da tabela `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `item_price` double NOT NULL,
  `item_descount` int(11) DEFAULT NULL,
  `item_description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_is_extra` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `item`
--

INSERT INTO `item` (`id`, `item_name`, `category_id`, `item_price`, `item_descount`, `item_description`, `item_is_extra`) VALUES
(1, 'Calabresa', 1, 28, 0, 'Calabre com cebola e azeitonas', 0),
(2, 'Carne', 3, 7.8, 4, 'Carne temperada', 0),
(3, 'Bacon', 6, 2.5, 0, 'Adicional de Bacon', 1),
(4, 'Alho', 1, 1, 0, '', 0),
(5, 'Cebola', 6, 1.5, 0, '', 1),
(6, 'Portuguesa', 1, 36, 5, 'Mussarela, Presunto, Ovos, Cebola e Azeitonas', 0),
(7, 'Coca Lata', 3, 6.5, 0, 'Coca de lata 350 ml', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_`
--

DROP TABLE IF EXISTS `order_`;
CREATE TABLE IF NOT EXISTS `order_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `costumer_id` int(11) NOT NULL,
  `order_price` double DEFAULT NULL,
  `payment_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Aberto',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `order_`
--

INSERT INTO `order_` (`id`, `costumer_id`, `order_price`, `payment_type`, `order_status`) VALUES
(1, 1, NULL, NULL, 'Fechado'),
(2, 1, NULL, NULL, 'Fechado'),
(3, 1, NULL, NULL, 'Fechado'),
(4, 1, NULL, NULL, 'Fechado'),
(7, 1, NULL, NULL, 'Fechado'),
(12, 1, 37.8, 'Dinheiro', 'Fechado'),
(13, 6, 101, 'Dinheiro', 'Fechado'),
(14, 6, 137, 'Dinheiro', 'Fechado'),
(15, 7, 97, 'Dinheiro', 'Fechado'),
(16, 7, 97, 'Dinheiro', 'Fechado'),
(17, 1, 30, 'Dinheiro', 'Fechado'),
(18, 1, 94, 'Dinheiro', 'Fechado'),
(19, 2, 97, 'Cartão', 'Fechado'),
(20, 5, 56.5, 'Dinheiro', 'Fechado'),
(21, 6, 221, 'Cartão', 'Fechado'),
(22, 1, 30, 'Dinheiro', 'Fechado'),
(23, 8, 105, 'Cartão', 'Fechado'),
(25, 9, 107.5, 'Cartão', 'Fechado'),
(26, 1, 30, 'Dinheiro', 'Fechado'),
(27, 1, NULL, NULL, 'Aberto');

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_item`
--

DROP TABLE IF EXISTS `order_item`;
CREATE TABLE IF NOT EXISTS `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `item_id`) VALUES
(16, 12, 1),
(17, 12, 2),
(18, 13, 1),
(19, 13, 6),
(20, 13, 6),
(21, 14, 1),
(22, 14, 6),
(23, 14, 6),
(24, 14, 6),
(25, 15, 1),
(26, 15, 1),
(27, 15, 6),
(28, 16, 1),
(29, 16, 1),
(30, 16, 6),
(31, 17, 1),
(32, 18, 1),
(33, 18, 1),
(34, 18, 6),
(35, 19, 1),
(36, 19, 1),
(37, 19, 6),
(38, 20, 1),
(39, 20, 1),
(40, 21, 1),
(41, 21, 1),
(42, 21, 1),
(43, 21, 1),
(44, 21, 6),
(45, 21, 6),
(46, 21, 6),
(47, 22, 1),
(48, 23, 1),
(49, 23, 6),
(50, 23, 6),
(53, 25, 1),
(54, 25, 6),
(55, 25, 6),
(56, 25, 7),
(57, 26, 1),
(58, 27, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_item_add`
--

DROP TABLE IF EXISTS `order_item_add`;
CREATE TABLE IF NOT EXISTS `order_item_add` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `user_email`, `user_password`) VALUES
(1, 'ewerton.azevedo@hotmail.com', '191190');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order_` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
