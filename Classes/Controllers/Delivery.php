<?php 

/**
 * 	
 */
class Delivery extends Controller
{
	use AuthService;
	use CostumerService;
	use AddressService;
	use ItemService;
	use OrderItemService;
	use OrderService;
	use OrderItemAddService;

	public function index()
	{	
		if(isset($_POST) && !empty($_POST))
		{
			
			$costumer = $this->getCostumerByNumber($_POST['costumer_number']);
			
			if($costumer != false)
			{
				$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Delivery', 'Views/static/systemhead.phtml');
			} else {
				$costumer_number = $_POST['costumer_number'];
				$created = $this->createCostumer($_POST);
				if($created)
				{
					$costumer = $this->getCostumerByNumber($costumer_number);
					$this->view->render('Views/auth/delivery/costumer.phtml', array('costumer' => $costumer, 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')),null , 'Delivery - Cadastro', 'Views/static/systemhead.phtml');
				} else {
					$this->view->render('Views/errors/index.phtml');
				}
				
			}
		} else {
			$this->view->render('Views/auth/delivery/index.phtml', array('auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Delivery', 'Views/static/systemhead.phtml');
		}
	}

	public function rollbackCostumer($costumer_id){
		$deleted = $this->deleteCostumer($costumer_id);
		header("Location: /delivery");
	}

	public function address()
	{
		if(isset($_POST) && !empty($_POST))
		{
			$created = $this->createAddress($_POST);
			if($created)
			{
				$costumer = $this->getCostumerById($_POST['costumer_id']);
				$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')));
			} else {
				$this->view->render('Views/errors/index.phtml');
			}
		} else {
			$this->view->render('Views/errors/index.phtml');
		}
	}

	public function address_edit($address_id)
	{
		$address = $this->getAddressById($address_id);
		$this->view->render('Views/auth/delivery/costumer.phtml', array('address' => $address, 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Endereço - Edição', 'Views/static/systemhead.phtml');
	}

	public function address_uptade()
	{
		if(isset($_POST) && !empty($_POST))
		{
			$this->updateAddress($_POST);
			$costumer = $this->getCostumerById($_POST['costumer_id']);
			$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Endereço - Alterado', 'Views/static/systemhead.phtml');
		}
	}

	public function order()
	{
		
			if(isset($_POST) && !empty($_POST))
			{
				$this->createOrder($_POST);
				$order = $this->getLastOpenOrderByCostumerId($_POST['costumer_id']);			
				$costumer = $this->getCostumerById($_POST['costumer_id']);
				$items = $this->getAllItems();			
				$this->view->render('Views/auth/delivery/order.phtml', array('order'=> $order, 'items' => $items,'costumer' => $costumer, 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Delivery - Order', 'Views/static/systemhead.phtml');
			} else {
				$this->view->render('Views/errors/index.phtml');
			}		
	}

	public function rollbackOrder($order_id){
		$this->order_cancel($order_id);
	}

	public function done()
	{
		if(isset($_POST) && !empty($_POST)){
			$order = $this->getOrderById($_POST['id']);
			$costumer = $order->belongsToCostumer();
			$order->update($_POST['id'], $_POST);
			$order = $this->getOrderById($_POST['id']);
			$this->view->render('Views/auth/delivery/print.phtml', array('order' => $order ,'costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Pedido - Nota', 'Views/static/systemhead.phtml');

		} else {
			$this->view->render('Views/errors/index.phtml');
		}
	}
	public function add_order_item($order_id)
	{
		if(isset($order_id) && !empty($order_id)){
			$order = $this->getOrderById($order_id);
			$costumer = $order->belongsToCostumer();
			$items = $this->getAllItems();
			$this->view->render('Views/auth/delivery/order.phtml', array('order'=> $order, 'items' => $items,'costumer' => $costumer, 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')));
		} else {
			$this->view->render('Views/errors/index.phtml');
		}
	}

	public function add_item()
	{
		if (isset($_POST) && !empty($_POST)) {
			$costumer = $this->getCostumerById($_POST['costumer_id']);
			unset($_POST['costumer_id']);
			$this->addOrderItems($_POST);
			$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Delivery - Add Item', 'Views/static/systemhead.phtml');
		} else {
			$this->view->render('Views/errors/index.phtml');
		}		
	}

	public function costumer($costumer_id){
		$costumer = $this->getCostumerById($costumer_id);
		if($costumer){
			$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer, 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')),null , 'Delivery', 'Views/static/systemhead.phtml');
		} else {
			$this->view->render('Views/errors/index.phtml');
		}
	}

	public function order_cancel($order_id)
		{
			$costumer = $this->getOrderById($order_id)->belongsToCostumer();
			$this->getOrderInstance()->delete($order_id);
			$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Delivery - Cancel', 'Views/static/systemhead.phtml');

		}

		public function choose_aditional($order_item_id)
		{
			$order_item = $this->getOrderItemById($order_item_id);
			$items = $this->getAllItemsWhere('item_is_extra', '1');		
			$this->view->render('Views/auth/delivery/aditional.phtml', array('items' => $items,'order_item' => $order_item,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Pedido - Adicional', 'Views/static/systemhead.phtml');
		}

		public function add_aditional_item()
		{
			if(isset($_POST) && !empty($_POST)){
				$this->createOrderItemAdd($_POST);
				$costumer = $this->getOrderItemAddById($this->getOrderItemAddInstance()->readLast()[0])->belongsToOrderItem()->belongsToOrder()->belongsToCostumer();				
				$this->view->render('Views/auth/delivery/index.phtml', array('costumer' => $costumer,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Pedido - Item Adicionado', 'Views/static/systemhead.phtml');
			} else {
				$this->view->render('Views/errors/index.phtml');
			}
		}
}
?>