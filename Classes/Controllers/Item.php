<?php 

/**
 * 
 */
class Item extends Controller
{
	use AuthService;
	use ItemService;
	use CategoryService;

	public function index()
	{
		$this->view->render('Views/auth/item/index.phtml', array('items' => $this->getAllItems(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Cardapio - Itens', 'Views/static/systemhead.phtml');
	}

	public function add()
	{
		$this->view->render('Views/auth/item/add.phtml', array('categories' => $this->getAllCategories(), 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Cartapio - Adicionar', 'Views/static/systemhead.phtml');
	}


	public function edit($item_id)
	{
		$this->view->render('Views/auth/item/add.phtml', array('categories' => $this->getAllCategories(), 'item' => $this->getItemById($item_id), 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Item - Edição', 'Views/static/systemhead.phtml');
	}

	public function delete($item_id)
	{
		$deleted = $this->deleteItem($item_id);
		$this->view->render('Views/auth/item/index.phtml', array('items' => $this->getAllItems(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml'), 'deleted' => $deleted ), null, 'Item - Deletado', 'Views/static/systemhead.phtml');
	}

	public function update()
	{
		if (isset($_POST) && !empty($_POST))
		{
			$isSuccess = $this->updateItem($_POST);
			$this->view->render('Views/auth/item/index.phtml', array('items' => $this->getAllItems(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml'), 'success' => $isSuccess ), null, 'Item - Atualizado', 'Views/static/systemhead.phtml');
		} else {
			header("Location: /item/add");
		}
	}

	public function save()
	{
		if (isset($_POST) && !empty($_POST)){

			$isSuccess = $this->createItem($_POST);
		
			$this->view->render('Views/auth/item/index.phtml', array('items' => $this->getAllItems(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml'), 'success' => $isSuccess ));
		} else {
			header("Location: /item/add");
		}
	}
}

?>