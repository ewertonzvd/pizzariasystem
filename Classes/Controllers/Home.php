<?php 


/**
 * Classe Controler
 */
class Home extends Controller
{	
	use AuthService;
	use UserService;

	public function index()
	{	
		$header = "Views/static/head.phtml";
		$title = "Login";
		if($this->verifySession()){
			$header = "Views/static/systemhead.phtml";
			$title = "System";
		}		
		$this->view->render('Views/auth/index.phtml', array('auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/home/index.phtml')), null, $title, $header);
	}

	public function system()
	{		
		$this->view->render('Views/auth/index.phtml', array('auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Painel', 'Views/static/systemhead.phtml');
	}

	public function create()
	{
		if (isset($_POST) && !empty($_POST)) {
			$this->createUser($_POST);
			header("Location: /");
		} else {
			$this->view->render('Views/home/create.phtml', null, null, 'Criar Conta');
		}
		
	}

	public function login()
	{		
		$header = "Views/static/head.phtml";	
		if($this->verifySession()){
			$header = "Views/static/systemhead.phtml";
		}	
		$this->view->render('Views/auth/index.phtml', array('auth' => $this->verifyCredentials($_POST['email'], $_POST['password']), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')),null, 'System', $header);
		header("Location: /");
	}	

	public function logout()
	{
		session_start();
		session_unset();
		session_destroy();
		header("Location: /");
	}

}