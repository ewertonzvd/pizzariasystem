<?php 

/**
 * 
 */
class Category extends Controller
{
	use AuthService;
	use CategoryService;

	public function index()
	{
		$this->view->render('Views/auth/category/index.phtml', array('categories' => $this->getAllCategories() ,'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')), null, 'Categoria', 'Views/static/systemhead.phtml');
	}

	public function add()
	{
		$this->view->render('Views/auth/category/add.phtml', array('auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')));
	}

	public function edit($category_id)
	{
		$this->view->render('Views/auth/category/add.phtml', array('category' => $this->getCategoryById($category_id), 'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml')));
	}

	public function delete($category_id)
	{
		$deleted = $this->deleteCategory($category_id);
		$this->view->render('Views/auth/category/index.phtml', array('categories' => $this->getAllCategories(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml'), 'deleted' => $deleted ));
	}

	public function update()
	{
		if (isset($_POST) && !empty($_POST))
		{
			$isSuccess = $this->updateCategory($_POST);
			$this->view->render('Views/auth/category/index.phtml', array('categories' => $this->getAllCategories(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml'), 'success' => $isSuccess ));
		} else {
			header("Location: /category/add");
		}
	}

	public function save()
	{
		if (isset($_POST) && !empty($_POST)) {
			$isSuccess = $this->createCategory($_POST);
			$this->view->render('Views/auth/category/index.phtml', array('categories' => $this->getAllCategories(),'auth' => $this->verifySession(), 'error' => array('isError' => !$this->verifySession(), 'errorView' => 'Views/auth/error.phtml'), 'success' => $isSuccess ));
		} else {
			header("Location: /category/add");
		}
	}
}

?>