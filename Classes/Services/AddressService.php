<?php 

trait AddressService
{

	public function getAddressInstance()
	{
		return new AddressModel();
	}

	public function createAddress($address_data)
	{	
		return $this->getAddressInstance()->insert($address_data);
	}

	public function updateAddress($data)
	{
		return $this->getAddressInstance()->update($data['id'], $data);
	}

	public function deleteAddress($address_id)
	{
		return $this->getAddressInstance()->delete($address_id);
	}

	public function getAddressById($address_id)
	{
		return $this->getAddressInstance()->readById($address_id, 'AddressModel');
	}

	public function getAddressWhere($collum, $value)
	{
		return $this->getAddressInstance()->readByAtributes($collum, $value, 'AddressModel');
	}
	
	public function getAllAddressesWhere($collum, $value)
	{
		return $this->getAddressInstance()->readAllWhere($collum, $value, 'AddressModel');
	}

	public function getAllAddresses()
	{
		return $this->getAddressInstance()->readAll('AddressModel');
	}

}

?>