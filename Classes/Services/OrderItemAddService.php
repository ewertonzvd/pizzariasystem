<?php 

trait OrderItemAddService
{

	public function getOrderItemAddInstance()
	{
		return new OrderItemAddModel();
	}

	public function createOrderItemAdd($orderItemAdd_data)
	{	
		return $this->getOrderItemAddInstance()->insert($orderItemAdd_data);
	}

	public function updateOrderItemAdd($data)
	{
		return $this->getOrderItemAddInstance()->update($data['id'], $data);
	}

	public function deleteOrderItemAdd($orderItemAdd_id)
	{
		return $this->getOrderItemAddInstance()->delete($orderItemAdd_id);
	}

	public function getOrderItemAddById($orderItemAdd_id)
	{
		return $this->getOrderItemAddInstance()->readById($orderItemAdd_id, 'OrderItemAddModel');
	}

	public function getAllOrderItemAdds()
	{
		return $this->getOrderItemAddInstance()->readAll('OrderItemAddModel');
	}

	public function getAllOrderItemAddsWhere($collum, $value)
	{
		return $this->getOrderItemAddInstance()->readAllWhere($collum, $value, 'OrderItemAddModel');
	}

	public function addOrderItemAdds($orderItemAdd_data)
	{
		$quantity = (int) $orderItemAdd_data['quantity'];
		unset($orderItemAdd_data['quantity']);
		for ($i=0; $i < $quantity; $i++) { 
			$this->createOrderItemAdd($orderItemAdd_data);
		}
	}


}

?>