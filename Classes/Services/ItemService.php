<?php 

trait ItemService
{

	public function getItemInstance()
	{

		return new ItemModel();
	}

	public function createItem($item_data)
	{	
		
		return $this->getItemInstance()->insert($item_data);
	}

	public function updateItem($data)
	{
		return $this->getItemInstance()->update($data['id'], $data);
	}

	public function deleteItem($item_id)
	{
		return $this->getItemInstance()->delete($item_id);
	}

	public function getItemById($item_id)
	{
		return $this->getItemInstance()->readById($item_id, 'ItemModel');
	}

	public function getAllItems()
	{
		return $this->getItemInstance()->readAll('ItemModel');
	}

	public function getAllItemsWhere($collum, $value)
	{
		return $this->getItemInstance()->readAllWhere($collum, $value, 'ItemModel');
	}

}

?>