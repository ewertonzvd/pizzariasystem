<?php 

trait CategoryService
{

	public function getCategoryInstance()
	{
		return new CategoryModel();
	}

	public function createCategory($category_data)
	{	
		return $this->getCategoryInstance()->insert($category_data);
	}

	public function updateCategory($data)
	{
		return $this->getCategoryInstance()->update($data['id'], $data);
	}

	public function deleteCategory($category_id)
	{
		return $this->getCategoryInstance()->delete($category_id);
	}

	public function getCategoryById($category_id)
	{
		return $this->getCategoryInstance()->readById($category_id, 'CategoryModel');
	}

	public function getAllCategories()
	{
		return $this->getCategoryInstance()->readAll('CategoryModel');
	}

}

?>