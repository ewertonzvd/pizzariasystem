<?php 

trait OrderService
{

	public function getOrderInstance()
	{
		return new OrderModel();
	}

	public function createOrder($order_data)
	{	
		return $this->getOrderInstance()->insert($order_data);
	}

	public function updateOrder($data)
	{
		return $this->getOrderInstance()->update($data['id'], $data);
	}

	public function deleteOrder($order_id)
	{
		return $this->getOrderInstance()->delete($order_id);
	}

	public function getOrderById($order_id)
	{
		return $this->getOrderInstance()->readById($order_id, 'OrderModel');
	}

	public function getAllOrders()
	{
		return $this->getOrderInstance()->readAll('OrderModel');
	}

	public function getAllOrdersWhere($collum, $value)
	{
		return $this->getOrderInstance()->readAllWhere($collum, $value, 'OrderModel');
	}

	public function getLastOpenOrderByCostumerId($costumer_id)
	{
		foreach($this->getAllOrdersWhere('costumer_id', $costumer_id) as $order){
			if($order->getOrder_status() == 'Aberto'){
				return $order;
			}
		}
		return false;
	}

}

?>