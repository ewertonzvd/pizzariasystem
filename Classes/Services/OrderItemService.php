<?php 

trait OrderItemService
{

	public function getOrderItemInstance()
	{
		return new OrderItemModel();
	}

	public function createOrderItem($orderItem_data)
	{	
		return $this->getOrderItemInstance()->insert($orderItem_data);
	}

	public function updateOrderItem($data)
	{
		return $this->getOrderItemInstance()->update($data['id'], $data);
	}

	public function deleteOrderItem($orderItem_id)
	{
		return $this->getOrderItemInstance()->delete($orderItem_id);
	}

	public function getOrderItemById($orderItem_id)
	{
		return $this->getOrderItemInstance()->readById($orderItem_id, 'OrderItemModel');
	}

	public function getAllOrderItems()
	{
		return $this->getOrderItemInstance()->readAll('OrderItemModel');
	}

	public function getAllOrderItemsWhere($collum, $value)
	{
		return $this->getOrderItemInstance()->readAllWhere($collum, $value, 'OrderItemModel');
	}

	public function addOrderItems($orderItem_data)
	{
		$quantity = (int) $orderItem_data['quantity'];
		unset($orderItem_data['quantity']);
		for ($i=0; $i < $quantity; $i++) { 
			$this->createOrderItem($orderItem_data);
		}
	}


}

?>