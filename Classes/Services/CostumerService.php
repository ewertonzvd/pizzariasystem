<?php 

trait CostumerService
{

	public function getCostumerInstance()
	{
		return new CostumerModel();
	}

	public function createCostumer($costumer_data)
	{	
		return $this->getCostumerInstance()->insert($costumer_data);
	}

	public function updateCostumer($data)
	{
		return $this->getCostumerInstance()->update($data['id'], $data);
	}

	public function deleteCostumer($costumer_id)
	{
		return $this->getCostumerInstance()->delete($costumer_id);
	}

	public function getCostumerById($costumer_id)
	{
		return $this->getCostumerInstance()->readById($costumer_id, 'CostumerModel');
	}

	public function getCostumerByNumber($costumer_number)
	{
		return $this->getCostumerInstance()->readByAtributes('costumer_number', $costumer_number, 'CostumerModel');
	}

	public function getAllCostumers()
	{
		return $this->getCostumerInstance()->readAll('CostumerModel');
	}

	function mask($val, $mask)
	{
		$maskared = '';
		$k = 0;
		for($i = 0; $i<=strlen($mask)-1; $i++){
			if($mask[$i] == '#'){
				if(isset($val[$k]))
					$maskared .= $val[$k++];
			}else{
				if(isset($mask[$i]))
					$maskared .= $mask[$i];
			}
		}
		return $maskared;
	}


}

?>