<?php 

trait UserService
{

	public function getUserInstance()
	{
		return new UserModel();
	}

	public function createUser($user_data)
	{	
		$this->getUserInstance()->insert($user_data);
	}

	public function getUserById($user_id)
	{
		return $this->getUserInstance()->readById($user_id, 'UserModel');
	}

	public function getAllUsers()
	{
		return $this->getUserInstance()->readAll('UserModel');
	}

}

?>