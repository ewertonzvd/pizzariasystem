<?php 

/**
 * 	Model básico de Usuario
 *  @param [protected] $[db_table] [<Nome da tabela do banco de dados>]
 *  @param [private] $[id] [<id do usuario>]
 *  @param [private] $[user_email] [<user_email do usuario>]
 *  @param [private] $[user_password] [<senha do usuario>]
 */
class UserModel extends Model
{
	
	
	protected $db_table = 'user';

	private $id;
	private $user_email;
	private $user_password;	

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getUser_email()
	{
		return $this->user_email;
	}

	public function setUser_email($user_email)
	{
		$this->user_email = $user_email;
	}

	public function getUser_password()
	{
		return $this->user_password;
	}

	public function setUser_password($user_password)
	{
		$this->user_password = $user_password;
	}	
			
}

?>