<?php 

/**
 * 
 */
class CostumerModel extends Model
{
	use CostumerService;
	use AddressService;
	use OrderService;
	
	protected $db_table = 'costumer';

	private $id;
	private $costumer_number;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getCostumer_number()
	{
		return $this->costumer_number;
	}

	public function setCostumer_number($costumer_number)
	{
		$this->costumer_number = $costumer_number;
	}
	
	//Retorna todos os endereços da instancia atual do cliente.
	public function hasManyAddress()
	{
		return $this->getAllAddressesWhere('costumer_id', $this->getId());
	}

	//Retorna 1 endereço da instancia atual do cliente.
	public function hasOneAddress()
	{
		return $this->getAddressWhere('costumer_id', $this->getId());
	}

	//Retorna o pedido aberto da instancia atual do cliente.
	public function hasOneOpenOrder()
	{
		return $this->getLastOpenOrderByCostumerId($this->id);
	}

	/*
	*Retorna todos os pedidos da instancia atual do cliente.
	public function hasManyOrders()
	{
		return $this->getAllOrdersWhere('costumer_id', $this->getId());
	}
	*/
}

?>