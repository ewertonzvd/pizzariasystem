<?php 

/**
 * 				
 */
class AddressModel extends Model
{
	use CostumerService;

	protected $db_table = 'address';

	private $id;
	private $address_street;
	private $address_number;
	private $address_complement;
	private $address_observation;
	private $address_tax;
	private $costumer_id;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)			
	{
		$this->id = $id;
	}

	public function getAddress_street()
	{
			return $this->address_street;
	}

	public function setAddress_street($address_street)
	{
		$this->address_street = $address_street;
	}

	public function getAddress_number()
	{
		return $this->address_number;
	}

	public function setAddress_number($address_number)
	{
		$this->address_number = $address_number;
	}

	public function getAddress_complement()
	{
		return $this->address_complement;
	}

	public function setAddress_complement($address_complement)
	{
		$this->address_complement = $address_complement;
	}

	public function getAddress_observation()
	{
		return $this->address_observation;
	}

	public function setAddress_observation($address_observation)
	{
		$this->address_observation = $address_observation;
	}

	public function getAddress_tax()
	{
		return $this->address_tax;
	}

	public function setAddress_tax($address_tax)
	{
		$this->address_tax = $address_tax;
	}

	public function getCostumer_id()
	{
		return $this->costumer_id;
	}

	public function setCostumer_id($costumer_id)
	{
		$this->costumer_id = $costumer_id;
	}

	//Retorna o cliente dono deste endereço
	public function belongsToCostumer()
	{
		return $this->getCostumerById($this->costumer_id);
	}
	
}

?>