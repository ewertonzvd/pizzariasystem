<?php 

/**
 * 
 */
class ItemModel extends Model
{
	use ItemService;
	use CategoryService;

	protected $db_table = 'item';

	private $id;
	private $item_name;
	private $category_id;
	private $item_price;
	private $item_descount;
	private $item_description;
	private $item_is_extra;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getItem_name()
	{
		return $this->item_name;
	}

	public function setItem_name($item_name)
	{
		$this->item_name = $item_name;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}

	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getItem_price()
	{
		return $this->item_price;
	}

	public function setItem_price($item_price)			
	{
		$this->item_price = $item_price;
	}

	public function getItem_descount()
	{
		return $this->item_descount;
	}

	public function setItem_descount($item_descount)
	{
		$this->item_descount = $item_descount;
	}

	public function getItem_description()
	{
		return $this->item_description;
	}

	public function setItem_description($item_description)
	{
			$this->item_description = $item_description;
	}

	public function getItem_is_extra():int
	{
		return $this->item_is_extra;
	}

	public function setItem_is_extra($item_is_extra)
	{
		$this->item_is_extra = $item_is_extra;
	}

	//Retorna a instancia desta classe em Array.
	public function toArray()
	{
		return array(
			'id' => $this->id,
			'item_name' => $this->item_name,
			'category_id' => $this->category_id,
			'item_price' => $this->item_price,
			'item_descount' => $this->item_descount,
			'item_description' => $this->item_description
		);
	}

	//Retorna a categoria da instancia atual desta classe.
	public function hasOneCategory()
	{
		return $this->getCategoryById($this->category_id);
	}

}

?>