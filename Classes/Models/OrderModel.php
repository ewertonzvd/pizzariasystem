<?php 

/**
 * 
 */
class OrderModel extends Model
{
	use CostumerService;
	use OrderItemService;
	use OrderService;

	protected $db_table = 'order_';

	private $id;
	private $costumer_id;
	private $order_price;
	private $payment_type;
	private $order_status;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getCostumer_id()
	{
		return $this->costumer_id;
	}

	public function setCostumer_id($costumer_id)
	{
		$this->costumer_id = $costumer_id;
	}

	public function getOrder_price()
	{
		return $this->order_price;
	}

	public function setOrder_price($order_price)
	{
			$this->order_price = $order_price;
	}

	public function getPayment_type()
	{
		return $this->payment_type;
	}

	public function setPayment_type($payment_type)
	{
		$this->payment_type = $payment_type;
	}

	public function getOrder_status()
	{
		return $this->order_status;
	}

	public function setOrder_status($order_status)
	{
		$this->order_status = $order_status;
	}

	//Retorna a instancia do cliente.
	public function belongsToCostumer()
	{
		return $this->getCostumerById($this->costumer_id);
	}

	//Retorna todas instancias de items.
	public function hasManyOrderItems()
	{
		return $this->getAllOrderItemsWhere('order_id', $this->id);
	}
}
?>