<?php 

/**
 * 
 */
class OrderItemAddModel extends Model
{
	use OrderItemService;
	use ItemService;
	use OrderItemAddService;

	protected $db_table = 'order_item_add';
	private $id;
	private $order_item_id;
	private $item_id;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getOrder_item_id()
	{
			return $this->order_item_id;
	}

	public function setOrder_item_id($order_item_add)			
	{
		$this->order_item_id = $order_item_id;
	}

	public function getItem_id()
	{
		return $this->item_id;
	}

	public function setItem_id($item_id)
	{
		$this->item_id = $item_id;
	}

	//Retorna a instancia do item que pertençe a Instancia do pedido.
	public function belongsToOrderItem()
	{
		return $this->getOrderItemById($this->order_item_id);
	}

	//Retorna a instancia do Item referente a instancia atual desta classe.
	public function hasOneItem()	
	{
		return $this->getItemById($this->item_id);
	}
}

?>