<?php 

/**
 * 
 */
class CategoryModel extends Model
{
	
	protected $db_table = 'category';

	private $id;
	private $category_name;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getCategory_name()
	{
		return $this->category_name;
	}

	public function setCategory_name($category_name)
	{
		$this->category_name = $category_name;
	}

	//Retorna esta instancia da classe em array
	public function toArray()
	{
		return array(
			'id' => $this->id,
			'category_name' => $this->category_name
		);
	}
}

?>