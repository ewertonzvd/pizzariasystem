<?php 

/**
 * 				
 */
class OrderItemModel extends Model
{
	
	use OrderItemService;
	use OrderService;
	use ItemService;
	use OrderItemAddService;
	
	protected $db_table = 'order_item';

	private $id;
	private $order_id;
	private $item_id;

	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getOrder_id()
	{
		return $this->order_id;
	}

	public function setOrder_id($order_id)			
	{
		$this->order_id = $order_id;
	}

	public function getItem_id()
	{
		return $this->item_id;
	}

	public function setItem_id($item_id)
	{
		$this->item_id = $item_id;
	}

	//Retorna a instancia do Pedido a qual a atual instancia dessa classe pertence.
	public function belongsToOrder()
	{
		return $this->getOrderById($this->order_id);
	}

	//Retorna a instancia do Item.
	public function hasOneItem()
	{
		return $this->getItemById($this->item_id);
	}

	//Retorna todas as instancias dos Adicionais deste Item.
	public function hasManyOrderItemAdd()
	{
		return $this->getAllOrderItemAddsWhere('order_item_id', $this->id);
	}
	

}

?>